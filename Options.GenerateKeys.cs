﻿using CommandLine;

namespace Atlassian.Jira.OAuth.CLI
{
    [Verb("get-keys", HelpText = "Generates Public/Private keys.")]
    public class GenerateKeysOptions
    {
    }
}
