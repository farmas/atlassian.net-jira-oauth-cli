﻿using CommandLine;

namespace Atlassian.Jira.OAuth.CLI
{
    [Verb("get-tokens", HelpText = "Generates OAuth tokens.")]
    public class GenerateTokensOptions : BaseOptions
    {
        [Option('k', "key", Required = true, HelpText = "The Consumer Key as defined in the OAuth incoming authentication section of the application link in JIRA.")]
        public string ConsumerKey { get; set; }

        [Option('s', "secret", Required = true, HelpText = "The Private Key that was used to generate the public key defiend in the OAuth incoming authentication section of the application link in JIRA.")]
        public string PrivateKey { get; set; }
    }
}
