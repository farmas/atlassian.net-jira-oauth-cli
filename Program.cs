﻿using CommandLine;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Atlassian.Jira.OAuth.CLI
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            await Parser.Default.ParseArguments<GenerateKeysOptions, GenerateTokensOptions, AppLinkSetup>(args)
               .MapResult(
                (GenerateKeysOptions opts) => PrintEncryptionKeys(),
                (GenerateTokensOptions opts) => GetAndPrintOAuthTokens(opts),
                (AppLinkSetup opts) => ManualSetup(opts),
                errs => Task.FromResult(1));
        }

        public static RSAKeys GenerateEncryptionKeys()
        {
            return CryptoServices.GenerateKeys();
        }

        private static Task<int> PrintEncryptionKeys()
        {
            var keys = GenerateEncryptionKeys();
            PrintHeader("Generating Public/Key Pair.");
            PrintKeyValuePair("Public Key:", keys.PublicKey, true);
            PrintKeyValuePair("Private Key:", keys.PrivateKey, true);
            return Task.FromResult(0);
        }

        private static Task ManualSetup(AppLinkSetup opts)
        {
            var keys = GenerateEncryptionKeys();

            PrintHeader("Create an App Link");
            Console.WriteLine("Go to https://developer.atlassian.com/server/jira/platform/oauth and follow instructions with the data below:");
            PrintKeyValuePair("Your Consumer Key is: ", opts.ConsumerKey);
            PrintKeyValuePair("Your Public Key is:", keys.PublicKey, true);
            Console.WriteLine();
            Console.WriteLine("Hit 'Enter' when you are done configuring the AppLink.");
            Console.ReadLine();

            var tokenOptions = new GenerateTokensOptions()
            {
                JiraUrl = opts.JiraUrl,
                Username = opts.Username,
                Password = opts.Password,
                ConsumerKey = opts.ConsumerKey,
                PrivateKey = keys.PrivateKey
            };

            return GetAndPrintOAuthTokens(tokenOptions);
        }

        private static async Task<int> GetAndPrintOAuthTokens(GenerateTokensOptions opts)
        {
            PrintHeader("Generating Request Token");

            var oAuthTokenSettings = new OAuthRequestTokenSettings(opts.JiraUrl, opts.ConsumerKey, opts.PrivateKey);

            var oAuthRequestToken = await OAuthTokenHelper.GenerateRequestTokenAsync(oAuthTokenSettings);

            if (oAuthRequestToken == null)
            {
                PrintError("Failed to retrieve request token. Make sure the App Link has been created with the correct Consumer Key and Public Key.");
                return 1;
            }

            PrintHeader($"Authorizing token '{oAuthRequestToken.AuthorizeUri}'");

            try
            {
                var jira = new JiraHtmlEmulator(opts.JiraUrl);
                jira.Login(opts.Username, opts.Password);
                jira.Authorize(oAuthRequestToken.AuthorizeUri);
            }
            catch
            {
                Console.WriteLine($"Unable to automatically authorise token, please visit '{oAuthRequestToken.AuthorizeUri}' in a browser.");
                Console.WriteLine("Hit 'Enter' when you are done authorizing the token.");
                Console.ReadLine();
            }

            PrintHeader("Generating Access Token");

            var oAuthAccessTokenSettings = new OAuthAccessTokenSettings(oAuthTokenSettings, oAuthRequestToken);
            var accessToken = await OAuthTokenHelper.ObtainAccessTokenAsync(oAuthAccessTokenSettings, CancellationToken.None);

            if (string.IsNullOrEmpty(accessToken))
            {
                PrintError("Failed to retrieve access token.");
                return 1;
            }

            PrintTokens(new TokenResults(opts.ConsumerKey, opts.PrivateKey, oAuthRequestToken, accessToken));

            return 0;
        }

        private static void PrintTokens(TokenResults results)
        {
            PrintKeyValuePair("Consumer Key: ", results.ConsumerKey);
            PrintKeyValuePair("Access Token: ", results.AccessToken);
            PrintKeyValuePair("Token Secret: ", results.TokenSecret);
            PrintKeyValuePair("Consumer Secret:", results.PrivateKey, true);
        }

        private static void PrintError(string error)
        {
            PrintWithColor(error, ConsoleColor.Red);
        }

        private static void PrintHeader(string header)
        {
            PrintWithColor(header, ConsoleColor.Green);
            Console.WriteLine();
        }

        private static void PrintWithColor(string val, ConsoleColor color)
        {
            var temp = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(val);
            Console.ForegroundColor = temp;
        }

        private static void PrintKeyValuePair(string key, string val, bool isMultiLine = false)
        {
            var temp = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.Write(key);
            if (isMultiLine) Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(val);

            Console.ForegroundColor = temp;
        }
    }
}
