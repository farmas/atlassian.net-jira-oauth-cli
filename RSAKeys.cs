﻿namespace Atlassian.Jira.OAuth.CLI
{
    public class RSAKeys
    {
        public string PrivateKey { get; set; }
        public string PublicKey { get; set; }
    }
}
