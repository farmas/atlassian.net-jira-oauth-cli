﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlassian.Jira.OAuth.CLI
{
    [Verb("setup", HelpText = "Guides you through the steps of setting up OAuth in Jira.")]
    public class AppLinkSetup : BaseOptions
    {
        [Option('k', "key", Required = true, HelpText = "The Consumer Key as defined in the OAuth incoming authentication section of the application link in JIRA.")]
        public string ConsumerKey { get; set; }
    }
}
