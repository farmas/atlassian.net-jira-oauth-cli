﻿using CommandLine;

namespace Atlassian.Jira.OAuth.CLI
{
    public class BaseOptions
    {
        [Option("url", Default = true, Required = true, HelpText = "Url to the JIRA server.")]
        public string JiraUrl { get; set; }

        [Option('u', "username", Required = true, HelpText = "The username of the user to get the OAuth tokens for.")]
        public string Username { get; set; }

        [Option('p', "password", Required = true, HelpText = "The password of the user.")]
        public string Password { get; set; }
    }
}
